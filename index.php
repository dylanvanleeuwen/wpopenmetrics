<?php
/*
Plugin name: WP Open Metrics
Plugin URI: http://www.wpopenmetrics.com
Description:
Author: Dylan van Leeuwen
Author URI: http://www.wpopenmetrics.com
Version: 0.1 (BETA)
*/

// the name of the settings page for the license input to be displayed
define( 'WPOMSetting', 'wpom-setting' );

function wpom_license_menu() {
    add_options_page( 'WP Open Metrics', 'WP Open Metrics', 'manage_options', WPOMSetting, 'WPOM_settings_page' );
}
add_action('admin_menu', 'wpom_license_menu');

function WPOM_settings_page(){
?>
    <h1>WP Open Metrics Settings</h1>
    <form method="post" action="options.php">
        <?php settings_fields( 'WPOM_post_settings' ); ?>
        <?php do_settings_sections( 'WPOM_post_settings' ); ?>
        <table class="form-table">
            <tr valign="top">
                <th scope="row">Consumer Key:</th>
                <td><input type="text" name="Consumer-key-api" value="<?php echo get_option( 'consumer-key-api' ); ?>"/></td>
            </tr>
            <tr valign="top">
                <th scope="row">Consumer Secret:</th>
                <td><input type="text" name="Consumer-secret-api" value="<?php echo get_option( 'consumer-secret-api' ); ?>"/></td>
            </tr>
        </table>
        <?php submit_button(); ?>
    </form>

    <?php
}

if( !function_exists("update_extra_post_info") ) {
    function update_extra_post_info() {
        register_setting( 'WPOM_post_settings', 'consumer-key-api' );
        register_setting( 'WPOM_post_settings', 'consumer-secret-api' );
    }
}
add_action( 'admin_init', 'update_extra_post_info' );

function wpom_dashboard_page()	{
    ?>
    <?php
}
add_shortcode('wpopenmetrics', 'wpom_dashboard_page');
